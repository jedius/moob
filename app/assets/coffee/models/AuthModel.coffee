AuthModel = Backbone.Model.extend
    initialize: ->
        @.on 'change:token', @update, @
        #console.log 'AuthModel init'
        #@getToken()
        #@update()
        #@trigger 'info'
        #if app.router.started
            #if @get('channels').length > 0
                #app.router.navigate 'my/news', trigger: true
            #else
                #app.router.navigate 'list/user/channel/1', trigger: true


    getToken: ->
        console.log 'AuthModel getToken token:', $.cookie('token')
        $.get '/auth/token', {token: $.cookie('token'), json: true}, (res)=>
            console.log 'get token', res
            if res and res.token
                @set 'token', res.token
            else
                console.log 'AuthModel initialize, no token!', res if app.debug

    update: ->
        if @get('token')
            console.log 'AuthModel update', @get 'token'
            $.cookie 'token', @get 'token'
            $.ajaxSetup
                beforeSend: (xhr)=>
                    xhr.setRequestHeader("Authorization", "Bearer "+@get('token'))
                    app.loading true
                #contentType: 'application/json;charset=UTF-8'
                #dataType: 'json'
            $.get '/api/v0/my/info', (res)=>
                #если ошибка о невалидности токена то отправляем на регистрацию
                console.log 'my info', res
                if !res.users
                    $.cookie 'token', null
                    window.location = '/'
                    
                if res.users and res.users[0]
                    @set 'user', res.users[0]
                    @set 'id', res.users[0].id

                if res.awarders and res.awarders[0]
                    @set 'awarder', res.awarders[0]

                if res.issuers and res.issuers[0]
                    @set 'issuer', res.issuers[0]

                    console.log 'AuthModel update, model:', @toJSON() if app.debug

                if app.router.started
                    if @get('user') and @get('user').channels.length > 0
                        app.router.navigate 'my/news', trigger: true
                    else
                        app.router.navigate 'list/user/channel/1', trigger: true
                @trigger 'info'
        else
            console.log 'no token'
            #console.log app.router.started
            #app.router.navigate 'auth/user', trigger: true
            @trigger 'ready'
            
    login: (data, cb) ->
        $.ajax
            type: "POST"
            url: "/auth/signIn/"
            data: data
            #data: JSON.stringify(data)
            #contentType: 'application/json;charset=UTF-8'
            #dataType: 'json'
            success: (res) =>
                console.log "login, response:", res if app.debug
                if res.token
                    @set 'token', res.token
                    cb false if cb
                else
                    console.log 'login fail, res:', res if app.debug
                    cb res if cb
            error: (e,err) =>
                cb err if cb

    join: (data, cb) ->
        console.log "join data:", data if app.debug
        if @get "login"
            $.post "/v0/user/#{@get('id')}", data, (res)=>
                if res.users and res.users[0]
                    @set 'user', res.users[0]
                else
                    app.popover res.error
                console.log @.toJSON()
        else
            $.ajax
                type: "POST"
                url: "/auth/signUp"
                data: data
                #contentType: 'application/json;charset=UTF-8'
                #dataType: 'json'
                #data: JSON.stringify(data)
                headers:
                    Authorization: @get('token')
                success: (res) =>
                    console.log "join response", res if app.debug
                    if res.token
                        if @get('token') isnt res.token
                            @set 'token', res.token
                        else
                            @update()
                        cb res if cb
                    else
                        console.log "res.value false" if app.debug
                        cb res if cb


    subscribe: (channelId)->
        console.log 'authModel subscribe as user, channelId:',channelId
        channels = @get('user').channels
        channels.push 
            _id: channelId 
            date: new Date(0)

        data = []
        for channel in channels
            data.push channel._id


        $.post "/api/v0/channel/user", {channels: data}, (res)=>
            console.log 'authModel subscribe res:', res if app.debug

            if res.status is 200 and res.users and res.users[0] 
                @set 'user', res.users[0]
                @trigger 'change:channels'


    unsubscribe: (channelId)->
        channels = @get('user').channels
        console.log ''
        index = false 
        for channel,i in channels
            if channel._id is channelId
                index = i
                break

        if index isnt false
            channels.splice(index,1)
            console.log "AuthModel unsubscribe channels:", channels

            data = []
            for channel in channels
                data.push channel._id

            console.log data
            $.post "/api/v0/channel/user", {channels: data}, (res)=>
                console.log 'authModel subscribe res:', res if app.debug

                if res.status is 200 and res.users and res.users[0] 
                    @set 'user', res.users[0]
                    @trigger 'change:channels'

    subscribeAsAwarder: (channelId)->
        console.log 'authModel subscribe as awarder, channelId:',channelId

        $.post "/api/v0/channel/#{channelId}/awarder", (res)=>
            console.log 'authModel subscribe res:', res if app.debug

            if res.status is 200 and res.awarders and res.awarders[0] and res.awarders[0].channels
                console.log 'authModel subscribe awarders.channels:',res.awarders[0].channels
                @set 'awarder', res.awarders[0]
                console.log 'AuthModel awarder channels after subscribe:',@get('awarder').channels
                @trigger 'change:channels'


    unsubscribeAsAwarder: (channelId)->
        console.log 'authModel unsubscribe as awarder, channelId:',channelId
        $.ajax
            url: "/api/v0/channel/#{channelId}/awarder/#{@get('id')}"
            type: 'DELETE'
            success: (res)=>
                console.log 'channelView unsubscribe awarder res:', res if app.debug
                if res.status is 200
                    channels = @get('awarder').channels or []
                    index = false
                    for channel,c in channels
                        if channel._id is channelId
                            index = c
                            break
                    console.log index
                    if index?
                        channels.splice index,1
                        awarder = @get 'awarder'
                        awarder.channels = channels
                        @set 'awarder', awarder
                        @trigger 'change:channels'



    defaults: 
        user:
            name: ''
            login: ''
            email: ''
            roles: []
            channels: []
            

