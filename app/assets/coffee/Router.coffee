Router = Backbone.Router.extend

    routes:
        home: 'home'
        'main':'home'

        'auth/:action': 'auth'
        
        logout: 'logout'

        'my/news': 'myNews'

        'channel/:id/:page': 'channel'
        'channel/:id/users/:page': 'channelUsers'
        
        'product/:id': 'product'
        'offer/:id': 'offer'

        'list/:role/:entity/:page': 'list'
        'list/:role/:entity/:page/:active': 'listActive'

        'swagger/:server':'swagger'         

        'my/:role/:entity/:page': 'changeChannel'

        'pt/:payway/:entity/:page': 'choosePaywayTemplate'
        
        'pm/:role/:entity': 'payment'
        
        'lt/:payway/:entity/:page': 'chooseLoyaltyTemplate'

        'un/:role/:entity': 'unicode'
       
        #Default
        "*actions": "defaultAction"


    initialize: ->
        console.log "router init" if app.debug
        @views = {}

    start: ->
        unless @started
            console.log "router start" if app.debug
            console.log '3'
            Backbone.history.start()
            @started = true

    logout: ->
        console.log 'router logout'
        #window.location = '/'
        $.cookie "token", null
        @views = {}
        app.model.clear()
        app.authModel.clear()
        app.render()
        @navigate 'auth/main', trigger: true
        #app.authModel.getToken()

    home: ->
        console.log 'Router home'
        app.$('.appModule').hide()
        unless @views.home
            @views.home = new HomeView
                el: $('<div id=home class=appModule />').appendTo(app.body)
        @views.home.render()
    

    auth: (action)->
        console.log 'Router auth, action:',action
        unless @views.auth
            console.log '4', app.body
            @views.auth = new AuthView
                model: app.authModel
                el: $('<div id=auth class=appModule />').appendTo(app.body)
                action: action
        else
            @views.auth.action = action
            @views.auth.render()
            @views.auth.show()

    myNews: ->
        console.log 'Router myNews'
        unless @views.myNews
            @views.myNews = new MyNewsView
                el: $('<div id=auth class=appModule />').appendTo(app.body)
                model: app.authModel
        @views.myNews.render()

    list: (role,entity,page)->
        console.log 'router list',entity,page
        @views.lists = {} unless @views.lists
        @views.lists[role] = {} unless @views.lists[role]
        unless @views.lists[role][entity]
            @views.lists[role][entity] = new EntityListView
                el: $('<div class=appModule />').appendTo(app.body)
                role: role
                page: page
                entity: entity
        else
            console.log '112323'
            @views.lists[role][entity].page = page
            @views.lists[role][entity].fetch()

    listActive: (role,entity,page,active)->
        console.log 'router listActive',entity,page,active
        @views.lists = {} unless @views.lists
        @views.lists[role] = {} unless @views.lists[role]
        unless @views.lists[role][entity]
            @views.lists[role][entity] = new EntityListView
                el: $('<div class=appModule />').appendTo(app.body)
                role: role
                page: page
                active: active
                entity: entity
        else
            @views.lists[role][entity].page = page
            @views.lists[role][entity].active = active
            @views.lists[role][entity].fetch()

    channel: (id,page)->
        console.log "router channel #{id} #{page}"
        page = 1 unless page
        @views.channels = {} unless @views.channels
        unless @views.channels[id]
            @views.channels[id] = new ChannelView
                el: $('<div id=channel class=appModule />').appendTo(app.body)
                page: page
                id: id
        else
            @views.channels[id].fetch()

    #swagger: (server)->
        #console.log "router open swagger for #{server}"
        #$.get "/#{server}/docs/", (res)->
            #console.log res


    #Open userlist of channel
    #For view by users
    #For administrate it by issuer: accept users subscribe request, etc.
    
    #channelId, current page
    channelUsers: (id,page)->
        page = 1 unless page
        console.log 'Router channelUsers view:',@views.channelUsers
        @views.channelUsers = {} unless @views.channelUsers
        console.log "Router channelUsers #{id} view:",@views.channelUsers[id]
        unless @views.channelUsers[id]
            @views.channelUsers[id] = new ChannelUsersView
                el: $('<div id=channel class=appModule />').appendTo(app.body)
                page: page
                id: id
        else
            console.log 'Router: view already exist'
            @views.channelUsers[id].fetch()




    product: (id)->
        console.log "router product #{id}"
        @views.products = {} unless @views.products
        unless @views.products[id]
            @views.products[id] = new ProductView
                el: $('<div id=channel class=appModule />').appendTo(app.body)
                id: id
        else
            @views.products[id].render()

    offer: (id)->
        console.log "router offer #{id}"
        @views.offers = {} unless @views.offers
        unless @views.offers[id]
            @views.offers[id] = new OfferView
                el: $('<div id=channel class=appModule />').appendTo(app.body)
                id: id
        else
            @views.offers[id].render()

    defaultAction: (actions) ->
        console.log "defaultAction",actions
        @navigate "auth/main", trigger: true

    changeChannel: (role, entity, page)->
        console.log 'router changeChannel',role, entity, page
        @views.changeChannel = {} unless @views.changeChannel
        @views.changeChannel[role] = {} unless @views.changeChannel[role]
        @views.changeChannel[role] = new ChangeChannelView
            el: $('<div class=appModule />').appendTo(app.body)
            entity: entity
            role: role
            page: page

    choosePaywayTemplate:(role, entity, page)->
        console.log 'router paywayTemplate',role, entity, page
        @views.paywayTemplate = {} unless @views.paywayTemplate
        @views.paywayTemplate[role] = {} unless @views.paywayTemplate[role]
        @views.paywayTemplate[role] = new PaywayTemlateView
            el: $('<div class=appModule />').appendTo(app.body)
            entity: entity
            role: role
            page: page

    unicode:(role, entity)->         
        console.log 'router unicode',role, entity
        @views.unicode = {} unless @views.unicode
        @views.unicode[role] = {} unless @views.unicode[role]
        @views.unicode[role][entity] = new UnicodeSearchView
            el: $('<div class=appModule />').appendTo(app.body)
            entity: entity
            role: role

    payment:(entity)->
        console.log 'router payment',entity
        @views.payment = {} unless @views.payment
        @views.payment = new PaymentView
            el: $('<div class=appModule />').appendTo(app.body)
            entity: entity

    chooseLoyaltyTemplate:(role, entity, page)-> 
        console.log 'router paywayTemplate',role, entity, page
        @views.loyaltyTemplate = {} unless @views.loyaltyTemplate
        @views.loyaltyTemplate[role] = {} unless @views.loyaltyTemplate[role]
        @views.loyaltyTemplate[role] = new LoyaltyTemplate
            el: $('<div class=appModule />').appendTo(app.body)
            entity: entity
            role: role
            page: page





