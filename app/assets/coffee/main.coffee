app = {}
$(document).ready ->
    app = new AppView
        el: $('<div id=app />').appendTo('body')
    app.start()
