PaymentView = Backbone.View.extend
    events: 
        "click #oneRec": "oneRec"     
        "click #secondRec": "secondRec"
        "click #savePayment": "save"


    initialize:(obj)->
        console.log 'PaymentView init:',obj
        @entity = obj.entity
        $.get "/templates/payment/addPayment.html", (html)=>
            console.log 'get template'
            #console.log entity
            @template = _.template(html)
            @render()
    
    fetch:(cb)->
        $.get "/api/v0/location", (res)=>
            console.log 'location: ',res
            @locations = res.locations
            cb()
        $.get "/api/v0/receipt", (res)=>
            console.log 'receipt: ',res
            @receipts = res.receipts
            cb()
        $.get "/api/v0/payway/paywayTemplate", (res)=>
            console.log 'paywayTemplate: ',res
            @payTempl = res.paywayTemplates
            cb()
        $.get "/api/v0/loyalty/loyaltyTemplate", (res)=>
            console.log 'loyaltyTemplate: ',res
            @loyalTempl = res.loyaltyTemplates
            cb()

    render: ->
        console.log 'renderPaymentView'
        $('.appModule').hide()
        if @template
            @fetch ()=>
                @$el.html @template
                    nls: NLS
                    entity: @entity
                    locationId:@locations
                    receiptId: @receipts
                    paywayTemplateId: @payTempl
                    loyaltyTemplateId: @loyalTempl    
        @$el.show()

    oneRec: (e)->
        e.preventDefault()
        if $('#newReceipt').attr('class') is 'hide'
            $('#newReceipt').attr('class', 'show')
            $('#findReceiptById').attr('class', 'hide')
            $('#lineForOne').attr('style', 'border-bottom: 5px solid #bababa; padding: 2px; display: inline-block;')
            $('#lineForSecond').attr('style', 'border-color: 5px solid whiteSmoke; padding: 2px; display: inline-block;')
        else
            $('#lineForOne').attr('style', 'border-color: 5px solid whiteSmoke; padding: 2px; display: inline-block;')
            $('#newReceipt').attr('class', 'hide') 
    secondRec: (e)->
        e.preventDefault()
        if $('#findReceiptById').attr('class') is 'hide'
            $('#findReceiptById').attr('class', 'show')
            $('#newReceipt').attr('class', 'hide')
            $('#lineForSecond').attr('style', 'border-bottom: 5px solid #bababa; padding: 2px; display: inline-block;')
            $('#lineForOne').attr('style', 'border-bottom: 5px solid whiteSmoke; padding: 2px; display: inline-block;')
        else
            $('#lineForSecond').attr('style', 'border-color: 5px solid whiteSmoke; padding: 2px; display: inline-block;')
            $('#findReceiptById').attr('class', 'hide') 

    save: (e)->
        e.preventDefault()
        payment = @$('form#formAddDataPayment').serializeObject()
        if  typeof (payment.acceptedPaywayIds) == 'string'
            payment.acceptedPaywayIds = []
            payment.acceptedPaywayIds.push @$('#acceptedPaywayIds:checked').val()
            console.log payment.acceptedPaywayIds
        
        if  typeof (payment.acceptedLoyaltyIds) == 'string'
            payment.acceptedLoyaltyIds = []
            payment.acceptedLoyaltyIds.push @$('#acceptedLoyaltyIds:checked').val()
            console.log  payment.acceptedLoyaltyIds 

        receipt = @$('form#formAddDataReceipt').serializeObject()
        receiptId = @$('#recid').val()
 
        if $('#newReceipt').attr('class') is 'show' 
            data = {payment,receipt}
        else 
            data = {payment,receiptId}
        
        if $('#findUserById').val() is ''
            console.log 'unknown'
            $.post "/api/v0/payment", data, (res)=>
                console.log data
                console.log 'PaymentView save, create response:',res 
                if !res.error
                    app.popover
                        code: 200
                        message: 'locator '+res.locator  
                else
                    app.popover 
                        message: res.error
        else
            console.log 'known'
            userId = @$('#findUserById').val()
            $.post "/api/v0/payment/user/"+userId, data, (res)=>
                console.log userId
                console.log data
                console.log 'PaymentView save, create response:',res
                if !res.error
                    app.popover
                        code: 200
                        message: 'locator '+res.locator
                else
                    app.popover 
                        message: res.error
        false


