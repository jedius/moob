EntityListView = Backbone.View.extend

    events:
        "click .showModal": "showModal"
        "click button.btn-create": "showModal"
        "submit form.save-form": "save"
        "click .delete": "delete"
        "click .pag": "pagination"
        "click .deleteAll": "deleteAll"
        "click .toggle-active": "reactive"
        "click #allApp": "allApp"
        "click .open-row": "openRow"

        "click .create-issuer": "createIssuer"
        "click .create-awarder": "createAwarder"

        "click .publish-modal-show": "showPublishModal"
        "click .publish-modal-accept": "publish"

        "click .issuer-subscribes-show": "showSubscribesModal"
        #"click .issuer-subscribes-accept": "subscribe"
        "submit form.subscribes": "subscribe"
        "click .showUsers": "showUsers"
    
    initialize: (obj)->
        console.log "EntityView init",obj
        @collection = new EntityCollection
        @entity = obj.entity
        @entitys = obj.entity+'s'
        console.log 
        if obj.entity is 'loyalty'
            @entitys = 'loyalties'
        else
            @entitys = obj.entity+'s' 
        @active = obj.active if obj.active?
        @role = obj.role
        console.log obj.role
        @page = obj.page or 1
        if @role is 'issuer'
            if @entity is 'product' or @entity is 'offer'
                @channels = false
                app.model.on 'channels', =>
                    @channels = false
                $.get "/templates/entities/#{@entity}/publishModal.html", (html)=>
                    @publishModalTemplate = _.template(html)
        if @role is 'issuer'
            if @entity is 'usser'
                @channels = false
                app.model.on 'channels', =>
                    @channels = false
                $.get "/templates/entities/#{@entity}/subscribesModal.html", (html)=>
                    @subscribesModalTemplate = _.template(html)


        $.get "/templates/entities/#{@entity}/modal.html", (html)=>
            @modalTemplate = _.template(html)

        $.get "/templates/entities/#{@entity}/list.html", (html)=>
            @template = _.template(html)
            @collection.on 'reset', @render, @
            @collection.on 'add', @render, @
            @fetch()

    fetch: ->
        hash = "list/#{@role}/#{@entity}/#{@page}"
        if @active is 'unactive'
            hash += "/unactive"
            active = false
        else if @active is 'active'
            hash += "/active"
            active = true
        app.router.navigate hash
        console.log 'entity: '+@entity
        
        if @entity is 'payway'
            console.log 'get with templateIdpayTempl'
            $.get "/api/v0/#{@entity}", {skip: (@page-1)*20, active: active, role: @role}, (res)=>
                console.log 'EntityView init, get list response:', res
                @count = res.count
                @pageCount = parseInt(@count/20)+1
                if res[@entitys]
                    @collection.reset res[@entitys]
                else if res.error
                    console.log 'EntityView fetch error', res.error
                else
                    console.log 'EntityView fetch response', res
            $.get "/api/v0/payway/paywayTemplate", (res)=>
                @paywayTemplate = res.paywayTemplates
                console.log 'payTempl', @paywayTemplate 

        else $.get "/api/v0/#{@entity}", {skip: (@page-1)*20, active: active, role: @role}, (res)=>
            console.log 'EntityView init, get list response:', res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res[@entitys] 
                @collection.reset res[@entitys]
            else if res.error
                console.log 'EntityView fetch error', res.error
            else
                console.log 'EntityView fetch response', res

    render: ->
        if @entity is 'payway'
            console.log 'EntityView render with payways'
            app.$('.appModule').hide()
            if @collection and @template
                console.log 'active: '+@active
                @$el.html @template
                    nls: NLS
                    utils: utils
                    entity: @entity
                    entities: @collection.toJSON()
                    pageCount: @pageCount
                    page: @page
                    active: @active
                    role: @role
                    user: app.authModel.toJSON()
                    templateId: @paywayTemplate 
                if @page <= 1
                    @$('.pag-left').addClass('disabled')
                if @page >= @pageCount
                    @$('.pag-right').addClass('disabled')
                if @page > 1 and @page < @pageCount
                    @$('.pag-left, .pag-right').removeClass('disabled')
        else
            console.log 'EntityView render'
            app.$('.appModule').hide()
            if @collection and @template
                console.log 'active: '+@active
                @$el.html @template
                    nls: NLS
                    utils: utils
                    entity: @entity
                    entities: @collection.toJSON()
                    pageCount: @pageCount
                    page: @page
                    active: @active
                    role: @role
                    user: app.authModel.toJSON()
                if @page <= 1
                    @$('.pag-left').addClass('disabled')
                if @page >= @pageCount
                    @$('.pag-right').addClass('disabled')
                if @page > 1 and @page < @pageCount
                    @$('.pag-left, .pag-right').removeClass('disabled')
            $.get "/api/v0/statistic/offers", (res)=>
                $("#totalGenerated.dial").val(res.statistic.totalGenerated).knob
                  min: 0
                  max: 40
                  width: 100
                  height: 100
                  fgColor: "#DDDDDD"
                  inputColor: "#888888"
                  skin: "tron"
                  readOnly: true
                $("#totalRedeemed.dial").val(res.statistic.totalRedeemed).knob
                  min: 0
                  max: 40
                  height: 100
                  width: 100
                  fgColor: "#0074CC"
                  inputColor: "#0074CC"
                  skin: "tron"
                  readOnly: true
        @$el.show()

    showModal: (e)->
        id = $(e.currentTarget).attr "_id"
        if id
            #paywayTemplate update
            if @entity is 'payway'
                $.get "/api/v0/payway/paywayTemplate", (res)=>
                    @paywayTemplate = res.paywayTemplates
                $.get "/api/v0/#{@entity}/#{id}", (res)=>
                    console.log 'EntityView showModal, get response:',res
                    if res[@entitys]? and res[@entitys][0]
                        @$('.el-modal').html @modalTemplate
                            nls: NLS
                            entity: res[@entitys][0]
                            utils: utils
                            templateId: @paywayTemplate 
                        @modal = @$('.modal-update').modal({ show: true, backdrop: true })
                        @dataPicker() 

            #update for other entity
            else $.get "/api/v0/#{@entity}/#{id}", (res)=>
                console.log 111111
                console.log 'EntityView showModal, get response:',res
                if res[@entitys]? and res[@entitys][0]
                    console.log 2222                    
                    @$('.el-modal').html @modalTemplate
                        nls: NLS
                        entity: res[@entitys][0]
                        utils: utils
                    @modal = @$('.modal-update').modal({ show: true, backdrop: true })
                    $("#expirationDate").datepicker()
                    $("#issuedDate").datepicker()

                #special for update offer
                else
                    @$('.el-modal').html @modalTemplate
                        nls: NLS
                        entity: res.offer
                        utils: utils
                    @modal = @$('.modal-update').modal({ show: true, backdrop: true })
                    $("#expirationDate").datepicker()
                    $("#issuedDate").datepicker()
                ##   

        else if @entity is 'payway'
            console.log 'payway'
            $.get "/api/v0/payway/paywayTemplate", (res)=>
                console.log 'EntityView showModal, get response:',res
                #if res[@entitys]? and res[@entitys][0]
                @$('.el-modal').html @modalTemplate
                    nls: NLS
                    entity: 
                        roles: []
                    templateId:res.paywayTemplates
                @modal = @$('.modal-update').modal({ show: true, backdrop: true })
                @dataPicker()                
        else if @entity is 'loyalty'
            console.log 'loyalty'
            $.get "/api/v0/loyalty/loyaltyTemplate", (res)=>
                console.log 'EntityView showModal, get response:',res
                #if res[@entitys]? and res[@entitys][0]
                @$('.el-modal').html @modalTemplate
                    nls: NLS
                    entity: 
                        roles: []
                    loyalTempl:res.loyaltyTemplates
                @modal = @$('.modal-update').modal({ show: true, backdrop: true })
                @dataPicker()                
        else
            console.log 'other entity'
            @$('.el-modal').html @modalTemplate
                nls: NLS
                entity: 
                    roles: []
            @modal = @$('.modal-update').modal({ show: true, backdrop: true })

        e.preventDefault()
        false
        @dataPicker()

    save: ->
        console.log 'EntityView save'
        data = @$('.modal form.save-form').serializeObject()

        if data.active is 'true' or data.active is true
            data.active = true
        else
            data.active = false
        
        if !data.public?
            data.public = false

        if data.public is "true"
            data.public = true

        if data.public is "false"
            data.public = false

        console.log 'save data:',data
        @modal.modal('hide').on 'hidden', =>

            if data.id
                console.log data.id
                console.log '1 '+data.public
                @collection.get(data.id).save data,
                    success: =>
                        @fetch()
                        app.model.trigger @entitys
                        app.popover
                            code: 200
                            message: 'success'
            else
                $.post "/api/v0/#{@entity}", data, (res)=>
                    console.log data
                    console.log '2 '+data.public
                    console.log 'EntityView save, create response:',res
                    if !res.error and res[@entitys]
                        newCollection = [res[@entitys][0]] if res[@entitys][0]
                        for ent in @collection.toJSON()
                            newCollection.push ent
                        @collection.reset newCollection
                        app.model.trigger @entitys
                        app.popover
                            code: 200
                            message: 'success'
                    else
                        app.popover 
                            message: res.error
        false

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'EntityView delete, id:', id
        if confirm 'are you sure?'
            @collection.get(id).destroy()
            @render()
            app.model.trigger @entitys
            app.popover
                code: 200
                message: 'success'
        e.preventDefault()
        false

    deleteAll: ->
        console.log 'EntityView deleteAll'
        if confirm "are you really want to delete all #{@entitys}?"
            @page = 1
            @pageCount = 1
            $.ajax
                type: 'DELETE'
                url: "/api/v0/#{@entity}?role=#{@role}"
                success: (res)=>
                    console.log 'deleteAll res',res
                    app.model.trigger @entitys
                    @fetch()
                    app.popover
                        code: 200
                        message: 'success'
    pagination: (e)->
        if $(e.currentTarget).attr('pag') is 'next'
            if @page < @pageCount
                @page++ 
                console.log 'pagination', @page
                @fetch()
        if $(e.currentTarget).attr('pag') is 'prev'
            if @page > 1
                @page--
                console.log 'pagination', @page
                @fetch()

    reactive: ->
        console.log 'entityView reactive',@active
        if @active is 'active'
            @active = 'unactive'
        else
            @active = 'active'
        @fetch()

    allApp: ->
        console.log 'EntityView all apps', @active
        hash = "list/#{@role}/#{@entity}/#{@page}"
        app.router.navigate hash
        $.get "/api/v0/#{@entity}", {skip: (@page-1)*20, role: @role}, (res)=>
            console.log 'EntityView all apps, get list response:', res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res[@entitys]
                @collection.reset res[@entitys]
            else if res.error
                console.log 'EntityView fetch error', res.error
            else
                console.log 'EntityView fetch response', res
    openRow: (e)->
        url = $(e.currentTarget).attr('_url')
        console.log 'EntityView openRow, url:',url
        if url
            app.router.navigate url, trigger: true

    getChannels: (cb)->
        unless @channels
            $.get "/api/v0/my/issuer/channels", {skip: 0, limit: 50, role: @role, select: 'id name'}, (res)=>
                console.log 'EntityView showPublishModal, get response:',res
                if res.channels
                    @channels = res.channels
                    cb()
                else
                    app.popover 
                        code: 400
                        message: 'no channels'
                        
        else
            cb()

    showPublishModal: (e)->
        console.log 'EntityListView showPublishModal'
        id = $(e.currentTarget).attr "_id"
        console.log 'id of changing user:', id
        if id and @publishModalTemplate
            @getChannels =>
                if @channels
                    @$('.el-publish-modal').html @publishModalTemplate
                        nls: NLS
                        channels: @channels
                        id: id
                    @publishModal = @$('.publish-modal').modal({ show: true, backdrop: true })
        e.preventDefault()
        false

    publish: (e)->
        console.log 'EntityListView publish'
        @publishModal.modal 'hide'
        data = @$('.publish-modal form').serializeObject()
        console.log 'EntityListView publish, data:',data
        $.post "/api/v0/#{@entity}/#{data.id}/publish", data, (res)=>
            console.log res
            app.model.trigger 'publish'
            app.popover
                code: 200
                message: 'success'
        e.preventDefault()
        false

    showSubscribesModal: (e)->
        console.log 'EntityListView showSubscribesModal'
        id = $(e.currentTarget).attr "_id"
        console.log 'id of user:', id
        if id and @subscribesModalTemplate
            @getChannels =>
                if @channels
                    @$('.el-subscribes-modal').html @subscribesModalTemplate
                        nls: NLS
                        channels: @channels
                        id: id
                    @subscribesModal = @$('.subscribes-modal').modal({ show: true, backdrop: true })
        e.preventDefault()
        false
        @dataPicker()
    subscribe: (e)->
        console.log 'EntityListView subscribe'
        @subscribesModal.modal 'hide'
        data = @$('.subscribes-modal form').serializeObject()
        if typeof data.channels is 'string'
            data.channels = [data.channels]

        console.log 'EntityListView subscribe, data:',data
        $.post "/api/v0/channel/user/#{data.id}",{channels: data.channels}, (res)=>
            console.log res
        e.preventDefault()
        false

    showUsers:(e)->
        console.log 'EntityListView showUsers'
        id = $(e.currentTarget).attr "_id"
        app.router.navigate "channel/#{id}/users/1", trigger: true
        e.preventDefault()
        false

    createAwarder:(e)->
        console.log 'EntityListView create awarder'
        userId = $(e.currentTarget).attr "userId"

        $.post "/api/v0/awarder/#{userId}", {active: true}, (res)=>
            console.log res
            @fetch()

        e.preventDefault()
        false

    createIssuer:(e)->
        console.log 'EntityListView create issuer'
        userId = $(e.currentTarget).attr "userId"

        $.post "/api/v0/issuer/#{userId}", {active: true}, (res)=>
            console.log res
            @fetch()

        e.preventDefault()
        false

    dataPicker: ->
        @myDate = new Date();
        @month = @myDate.getMonth() + 1;
        @creatDate = @month + '/' + @myDate.getDate() + '/' + @myDate.getFullYear()
        $("#expirationDate").val(@creatDate);
        $("#issuedDate").val(@creatDate);
        $("#expirationDate").datepicker()
        $("#issuedDate").datepicker()




