OfferView = Backbone.View.extend

    events:
        "click .showModal": "showModal"
        #"click .rules": "showRulesForm"
        "click .getUnicode": "getUnicode"
        "submit form.save-form": "save"
        "click .delete": "delete"

    initialize: (obj)->
        console.log 'OfferView init' if @debug
        @collection = new OfferRuleCollection
        #@collection.on 'reset', @render, @
        #@collection.on 'add', @render, @
        @id = obj.id
        @offer =
            rules: []
        $.get '/templates/offer/modal.html', (html)=>
            @modalTemplate = _.template(html)
            if @modalTemplate and @template and @ruleTemplate
                @render()
        $.get '/templates/offer/offer.html', (html)=>
            @template = _.template(html)
            if @modalTemplate and @template and @ruleTemplate
                @render()
        $.get '/templates/offer/rule.html', (html)=>
            @ruleTemplate = _.template(html)
            if @modalTemplate and @template and @ruleTemplate
                @render()

    fetch: (cb)->
        console.log 'FETCH'
        $.get "/api/v0/offer/#{@id}", (res)=>
            if res.status is 200
                @offer = res.offer
                @collection.reset @offer.rules
                @offerGiven = true
                if @offerGiven? and @staticticGiven?
                    cb()
        $.get "/api/v0/statistic/#{@id}", (res)=>
            if res.status is 200
                #$("#generated.dial").val(res.statistic.generated)
                #$("#redeemed.dial").val(res.statistic.redeemed)
                @generated = res.statistic.generated
                @redeemed = res.statistic.redeemed
                @staticticGiven = true
                if @offerGiven? and @staticticGiven?
                    cb()
    render: ->
        @fetch ()=>
            console.log 'OfferView render'
            console.log @template
            console.log @ruleTemplate
            if @template and @ruleTemplate
                    console.log 'offer'
                    if app.authModel.get('user').roles.indexOf('admin') isnt -1
                        role = 'admin'
                    else if app.authModel.get('id') is @offer.issuerId
                        role = 'issuer'
                    else
                        role = 'user'
                    @$el.html @template
                        nls: NLS
                        offer: @offer
                        role: role
                        utils: utils
                    if @offer.rules
                        #console.log @offer.rules
                        for rule,r in @offer.rules
                            html = @ruleTemplate
                                nls: NLS
                                offer: @offer
                                rule: rule
                                role: role
                                utils: utils
                            @$('.offer-rules-list').prepend html
                    $('.appModule').hide()
                    console.log "generated"
                    console.log @generated
                    $("#generated.dial").val(@generated).knob
                      min: 0
                      max: 40
                      width: 100
                      height: 100
                      fgColor: "#DDDDDD"
                      inputColor: "#888888"
                      skin: "tron"
                      readOnly: true
                    $("#redeemed.dial").val(@redeemed).knob
                      min: 0
                      max: 40
                      height: 100
                      width: 100
                      fgColor: "#0074CC"
                      inputColor: "#0074CC"
                      skin: "tron"
                      readOnly: true
                    @$el.show()

    showModal: (e)->
        id = $(e.currentTarget).attr "_id"
        console.log id
        if id
            rule = @collection.get(id).toJSON()
        else
            rule = {}
        @$('.el-modal').html @modalTemplate
            offer: @offer
            rule: rule
            utils: utils
            nls: NLS

        myDate = new Date();
        month = myDate.getMonth() + 1;
        prettyDate = month + '/' + myDate.getDate() + '/' + myDate.getFullYear();
        $(".anytime").val(prettyDate);
        $(".anytime").datepicker()
        @modal = @$('.modal').modal({ show: true, backdrop: true })
        e.preventDefault()
        false

    save: (e)->
        id = $(e.currentTarget).attr "_id"
        data = @$('.modal form').serializeObject()
        console.log 'OfferView save, data:', data, id
        @modal.modal('hide').on 'hidden', =>
            if data.id
                url = "/api/v0/offer/#{@id}/rule/#{data.id}"
            else
                url = "/api/v0/offer/#{@id}/rule"
            $.post url, data, (res)=>
                console.log 'OffersView save, response:',res
                if res.offers and res.offers[0]
                    @render()
                else
                    app.popover res.error
        e.preventDefault()
        false

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'OfferView delete, id:', id
        if confirm 'are you sure?'
            #@collection.get("/api/v0/offer/#{@id}/rule/id").destroy()
            $.ajax
                type: 'DELETE'
                url: "/api/v0/offer/#{@id}/rule/"+id
                success: (res)=>
                    @render()
            #$.destroy '/api/v0/offer/#{@id}/rule/id/'


            #@render()
        false

    getUnicode: (e)->
        $.post "/api/v0/offer/#{@id}/unicode", (res)=>
            console.log 'OfferView getUnicode response', res
            if res.unicode
                @code = res.unicode.code
                @render()
                app.popover 
                    code: 200
                    message: 'code: '+@code
        e.preventDefault()
        false
