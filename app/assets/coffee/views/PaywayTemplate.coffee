PaywayTemlateView = Backbone.View.extend
    
    events:
    
        "click button.btn-create": "showModal"
        "click .showModal": "showModal"
        "submit form.save-form": "save"
        "click .delete": "delete"
    
    initialize:(obj) ->
        console.log "paywayTemplateView init", obj
        @collection = new PaywayTemplateCollection
        @entity = obj.entity
        @entitys = obj.entity+'s'
        @active = obj.active if obj.active?
        @role = obj.role
        @page = obj.page or 1
        
        $.get "/templates/entities/#{@entity}/modal.html", (html)=>
            @modalTemplate = _.template(html)

        $.get "/templates/entities/#{@entity}/list.html", (html)=>
            @template = _.template(html)
            @collection.on 'reset', @render, @
            @collection.on 'add', @render, @
            @fetch()

    fetch: ->
        hash = "pt/#{@role}/#{@entity}/#{@page}"
        if @active is 'unactive'
            hash += "/unactive"
            active = false
        else if @active is 'active'
            hash += "/active"
            active = true
        app.router.navigate hash
        $.get "/api/v0/payway/#{@entity}", {skip: (@page-1)*20, active: active, role: @role}, (res)=>
            console.log 'PaywayTemplateView init, get list response:', res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res[@entitys]
                @collection.reset res[@entitys]
            else if res.error
                console.log 'PaywayTemplateView fetch error', res.error
            else
                console.log 'PaywayTemplateView fetch response', res

    showModal: (e)->
        id = $(e.currentTarget).attr "_id"
        if id
            $.get "/api/v0/payway/#{@entity}/#{id}", (res)=>
                console.log 'EntityView showModal, get response:',res
                if res[@entitys]? and res[@entitys][0]
                    @$('.el-modal').html @modalTemplate
                        nls: NLS
                        entity: res[@entitys][0]
                        utils: utils
                    @modal = @$('.modal-update').modal({ show: true, backdrop: true })
        else
            @$('.el-modal').html @modalTemplate
                nls: NLS
                entity: 
                    roles: []
            @modal = @$('.modal-update').modal({ show: true, backdrop: true })

        e.preventDefault()
        false
        @dataPicker()

    render: ->
        console.log 'EntityView render'
        app.$('.appModule').hide()
        if @collection and @template
            @$el.html @template
                nls: NLS
                utils: utils
                entity: @entity
                entities: @collection.toJSON()
                pageCount: @pageCount
                page: @page
                active: @active
                role: @role
                user: app.authModel.toJSON()
            if @page <= 1
                @$('.pag-left').addClass('disabled')
            if @page >= @pageCount
                @$('.pag-right').addClass('disabled')
            if @page > 1 and @page < @pageCount
                @$('.pag-left, .pag-right').removeClass('disabled')

        @$el.show()

    save: ->
        console.log 'EntityView save'
        data = @$('.modal form').serializeObject()

        if data.active is 'true' or data.active is true
            data.active = true
        else
            data.active = false

        if data.public is "true"
            data.public = true

        if data.public is "false"
            data.public = false

        console.log 'save data:',data
        @modal.modal('hide').on 'hidden', =>

            if data.id
                @collection.get(data.id).save data,
                    success: =>
                        @fetch()
                        app.model.trigger @entitys
            else
                $.post "/api/v0/payway/#{@entity}", data, (res)=>
                    console.log 'EntityView save, create response:',res
                    if !res.error and res[@entitys]
                        newCollection = [res[@entitys][0]] if res[@entitys][0]
                        for ent in @collection.toJSON()
                            newCollection.push ent
                        @collection.reset newCollection
                        app.model.trigger @entitys
                    else
                        app.popover res.error
        false

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'EntityView delete, id:', id
        if confirm 'are you sure?'
            @collection.get(id).destroy()
            @render()
            app.model.trigger @entitys
        e.preventDefault()
        false

