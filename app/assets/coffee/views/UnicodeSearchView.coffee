UnicodeSearchView = Backbone.View.extend

    events:
        "keyup #unicodeSearch": "buttonToggle"
        "click #takeUnicode": "search"
        'click #unicode-block': 'block'
        'click #unicode-redeem': 'redeem'
        'click #unicode-unblock': 'unblock'
 
    initialize: (obj)->
        console.log 'UnicodeView init', obj 
        @entity = obj.entity
        @role = obj.role
        $.get '/templates/unicode/search.html', (html)=>
            @template = _.template(html)
            @render()

        $.get '/templates/unicode/result.html', (html)=>
            @resultTpl = _.template(html)

    fetch: (code)->
        hash = "un/#{@role}/#{@entity}"
        app.router.navigate hash

        $.get "/api/v0/unicode/code/" + code, (data)=>
            console.log data
            if !data.error? 
                if data.unicode?
                    @unicode = data.unicode
                    @render()
                else
                    app.popover
                        message: 'Unicode not found'
                        @render()
            else
                app.popover data.error
                $('#result', @el).addClass('hide')
                @render()


    render: ->
        console.log 'UnicodeView render'
        $('.appModule').hide()

        if @template
            @$el.html @template
                entity: @entity
                role: @role
                nls: NLS

        if @resultTpl
            @$el.html @resultTpl
                unicode: @unicode
                entity: @entity
                utils: utils
                role: @role
                nls: NLS
        @$el.show()

    buttonToggle: (e)->
        if $(e.target).val() isnt ''
            $('#takeUnicode', @el).removeAttr('disabled')
        else
            $('#takeUnicode', @el).attr('disabled', true)

    search: (e)->
        e.preventDefault()
        code = $(e.target).prev(':input').val()
        @fetch(code)


    block: (e)->
        $.post '/api/v0/unicode/' + @unicode.id + '/block', (res)=>
            console.log res
            if res.status is 200
                if res.unicode?
                    @unicode = res.unicode 
                    @render()

            #if $(e.target).data('type') is 'block'
                #$(e.target).text('Unblock').data 'type', 'unblock'
                #$('#statusUnicode .status', @el).text 'Blocked'
                #$('#redeem', @el).removeClass('hide')
            #else
                #$(e.target).text('Block').data 'type', 'block'
                #$('#statusUnicode .status', @el).text 'Available'
                #$('#redeem', @el).addClass('hide')

    redeem: (e)->
        $.post '/api/v0/unicode/' + @unicode.id + '/redeem', (res)=>
            console.log res
            if res.status is 200
                if res.unicode?
                    @unicode = res.unicode 
                    @render()

            #if $(e.target).data('type') is 'redeem'
                #$('#statusUnicode .status', @el).text 'redeemed'
                #$('#block', @el).addClass('hide')
                #$('#redeem', @el).addClass('hide')

            #else
                #app.popover res.error
    unblock: (e)->
        $.post '/api/v0/unicode/' + @unicode.id + '/unblock', (res)=>
            console.log res

            if res.status is 200
                if res.unicode?
                    @unicode = res.unicode 
                    @render()
            #if $(e.target).data('type') is 'redeem'
                #$('#statusUnicode .status', @el).text 'redeemed'
                #$('#block', @el).addClass('hide')
                #$('#redeem', @el).addClass('hide')

            #else
                #app.popover res.error

            




