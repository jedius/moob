
#
#
#

ChannelUsersView = Backbone.View.extend

    initialize: (obj)->
        console.log 'ChannelUsersView initialize'
        @collection = new EntityCollection
        #@entity = 'user'
        @collection.on 'reset', @render, @
        @collection.on 'add', @render, @

        $.get '/templates/channelUsers/channelUsers.html', (html)=>
            @template = _.template(html)
            @page = obj.page or 1
            @id = obj.id
            #@fetch() if @channel

        $.get "/api/v0/channel/#{@id}", (res)=>
            console.log 'ChannelView getChannel res:',res
            
            @channel = res.channels[0]
            @fetch() if @template


    fetch: ->
        console.log 'ChannelUsersView fetch'
        app.router.navigate "channel/#{@id}/users/#{@page}"

        #get list of users, subscribed on the channel
        $.get "/api/v0/channel/#{@id}/users",{skip: (@page-1)*20}, (res)=>
            console.log 'ChannelUsersView get list of users, res:',res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res.channelUsers
                @collection.reset res.channelUsers

    render: ->
        console.log 'Userlist render'
        console.log 'session:', app.authModel
        console.log 'channel:', @channel

        if @collection and @template
            @$el.html @template
                nls: NLS
                utils: utils
                page: @page
                pageCount: @pageCount
                channel: @channel
                channelUsers: @collection.toJSON()
                role: 'issuer'
                session: app.authModel.get 'user'


            if @page <= 1
                @$('.pag-left').addClass('disabled')
            if @page >= @pageCount
                @$('.pag-right').addClass('disabled')
            if @page > 1 and @page < @pageCount
                @$('.pag-left, .pag-right').removeClass('disabled')
        $('.appModule').hide()
        @$el.show()

