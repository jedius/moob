ChangeChannelView = Backbone.View.extend
    events:

        "click .open-row": "openRow"
        "click .showModal": "showModal"
        "click button.btn-create": "showModal"
        "submit form.save-form": "save"
        "click .delete": "delete"
        "click .showUsers": "showUsers"
        "click .deleteAll": "deleteAll"

    initialize: (obj)->
        console.log 'ChangeChannelView init'
        @role = obj.role
        @active = obj.active if obj.active?
        @entity = obj.entity
        @entitys = obj.entity+'s'
        console.log '!!',@role, @entity,@entitys, 'in view'
        @collection = new ChangeChannelCollection 
        @page = obj.page or 1
        $.get "/templates/entities/channel/list.html", (html)=>
            @template = _.template(html)
            @collection.on 'reset', @render, @
            @collection.on 'add', @render, @
            @fetch()
        
        $.get "/templates/entities/channel/modal.html", (html)=>
            @modalTemplate = _.template(html)

    fetch: ->
        $.get "/api/v0/#{@role}/channels", {skip: (@page-1)*20, role: @role}, (res)=>
            console.log 'ChangeChannelView init, get list response:', res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res.channels
                @collection.reset res.channels
            else if res.error
                console.log 'ChangeChannelView fetch error', res.error
            else
                console.log 'ChangeChannelView fetch response', res
    
    render: ->
            console.log 'ChangeChannelView render'
            console.log '!!', @collection.toJSON()
            app.$('.appModule').hide()
            if @collection and @template
                @$el.html @template
                    nls: NLS
                    utils: utils
                    entity: 'channel'
                    entities: @collection.toJSON()
                    pageCount: @pageCount
                    active: @active
                    page: @page
                    role: @role
                    user: app.authModel.toJSON()
                if @page <= 1
                    @$('.pag-left').addClass('disabled')
                if @page >= @pageCount
                    @$('.pag-right').addClass('disabled')
                if @page > 1 and @page < @pageCount
                    @$('.pag-left, .pag-right').removeClass('disabled')

            @$el.show()

    showModal: (e)->
        id = $(e.currentTarget).attr "_id"
        if id
            $.get "/api/v0/channel/#{id}", (res)=>
                console.log 'EntityView showModal, get response:',res
                if res[@entity]? and res[@entity][0]
                    @$('.el-modal').html @modalTemplate
                        nls: NLS
                        entity: res[@entity][0]
                        utils: utils
                    @modal = @$('.modal-update').modal({ show: true, backdrop: true })
        else
            @$('.el-modal').html @modalTemplate
                nls: NLS
                entity: 
                    roles: []
            @modal = @$('.modal-update').modal({ show: true, backdrop: true })
        e.preventDefault()
        false

    save: ->
        console.log 'Change channel save'
        data = @$('.modal form').serializeObject()

        if data.active is 'true' or data.active is true
            data.active = true
        else
            data.active = false

        if data.public is "true"
            data.public = true

        if data.public is "false"
            data.public = false

        console.log 'save data:',data
        @modal.modal('hide').on 'hidden', =>
            if data.id
                @collection.get(data.id).save data,
                    success: =>
                        @fetch()
                        app.model.trigger @entitys
            else
                $.post "/api/v0/channel", data, (res)=>
                    console.log 'EntityView save, create response:',res
                    if !res.error and res.channels
                        newCollection = res.channels if res.channels
                        for ent in @collection.toJSON()
                            newCollection.push ent
                        @collection.reset newCollection
                        app.model.trigger @entity
                    else
                        app.popover res.error
        false

    openRow: (e)->
        url = $(e.currentTarget).attr('_url')
        console.log 'ChangeChannel openRow, url:',url
        if url
            app.router.navigate url, trigger: true

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'ChangeChannel, id:', id
        if confirm 'are you sure?'
            @collection.get(id).destroy()
            @render()
            app.model.trigger @entitys
            app.popover
                code: 200
                message: 'success'
        e.preventDefault()
        false

    deleteAll: ->
        console.log 'ChangeChannelViewView deleteAll'
        if confirm "are you really want to delete all channel?"
            @page = 1
            @pageCount = 1
            $.ajax
                type: 'DELETE'
                url: "/api/v0/channel?role=#{@role}"
                success: (res)=>
                    console.log 'deleteAll res',res
                    @fetch()
                    app.popover
                        code: 200
                        message: 'success'
    showUsers:(e)->
        console.log 'EntityListView showUsers'
        id = $(e.currentTarget).attr "_id"
        app.router.navigate "channel/#{id}/users/1", trigger: true
        e.preventDefault()
        false

    
