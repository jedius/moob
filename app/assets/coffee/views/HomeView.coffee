HomeView = Backbone.View.extend


    initialize: ->
        console.log 'HomeView init' if app.debug
        $.get '/templates/home.html', (html)=>
            @template = _.template(html)
            @$el.html @template nls: NLS

    render: ->
        console.log 'HomeView render' if app.debug
        app.$('.appModule').hide()
        @$el.show()


