MainAuthView = Backbone.View.extend
    
    events:
        "click #moobBut":"moobShow"
        "click #moobMeBut":"moobMeShow"
        "click #googleServBut":"googleServShow"
        "click #linkedBut":"linkInShow"
        "click #twitBut":"twitterShow"
        "click #facebookBut":"facebookShow"

        "click #signIn":"signIn"
        "click #login-twitter": "loginTwitter"
        "click #login-google": "loginGoogle"
        "click #login-linked-in": "loginLinkedIn"
        "click #login-facebook": "loginFacebook"
        "click #forgotPass": "showForgotPass"
        "click #passForgot": 'forgotPass'

    initialize:(obj) ->
        console.log 'MainAuthView init',obj.el
        @authtriger = true
        $.get '/templates/auth/mainAuth.html', (html)=>
            @template = _.template(html)
            if @template
                @render()
                if @authtriger 
                    @onLoad()
    render: ->
        if !@rendered and @template
            @$el.html @template
                nls: NLS
            @rendered = true
    show: ->
        if @render
            @$el.show()
            #@onLoad()

    signIn: (e)-> 
        e.preventDefault()

        data =
            login: $('#username').val()
            password: $('#password').val()
        console.log data
        console.log @model
        if data
            @model.login data, (err) =>
                console.log "Login has errors", err if err

        false

    loginLinkedIn: ->
        console.log 'LoginView loginLinkedIn'
        window.location = "/oauth/linkedin?token=#{@model.get('token')}"
        false

    loginTwitter: ->
        console.log 'LoginView loginTwitter'
        window.location = "/oauth/twitter?token=#{@model.get('token')}"
        false

    loginFacebook: ->
        console.log 'LoginView loginFacebook'
        window.location = "/oauth/facebook?token=#{@model.get('token')}"
        false

    loginGoogle:->
        console.log 'login google'
        window.location = "/oauth/google?token=#{@model.get('token')}"
        false

    onLoad: ->
        $('#moobLi').addClass('activeLoginlft-left')
        $('.authChose > H2').hide()
        $('.authChose > .normal > .normalContainer').hide()
        $("#moobBut").css('background-position','100% -60px')
        $('#moob > H2').fadeIn()
        $('#moob > .normal > .normalContainer').fadeIn()
        $('.chooseBut').css('z-index':'7')
        @authtriger = false
                

    moobShow: ->
        $('.fld').show()
        $('.forgotPass').hide();
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('#moobLi').addClass('activeLoginlft-left')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#moobBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#moob > H2').fadeIn(500)
        $('#moob > .normal > .normalContainer').fadeIn(500)
        $('#moob').css('z-index':'6')
        $('.chooseBut').css('z-index':'7')
        $('#moobLi').addClass('.activeLoginlft-left')

    moobMeShow: ->
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('#moobMeLi').addClass('activeLoginlft-left')
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#moobMeBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#moobMe > H2').fadeIn(500)
        $('#moobMe > .normal > .normalContainer').fadeIn(500)
        $('#moobMe').css('z-index':'6')

        console.log 'getting qr'
        $.get '/qr', (res)=>
            console.log 'loginwithqr res:',res
            #if res.token
            if res.challenge and res.sessionKey
                #@qrCode = res.token
                @challenge = res.challenge
                @sessionKey = res.sessionKey

                console.log 'AuthView login with qr'
                @socket = io.connect(/qr-session/+@sessionKey)
                @socket.on 'connect', (socket)=>
                    console.log 'new connection'
                    @socket.on 'success', (body)=>
                        @model.set('token', body.token)
                        @socket.disconnect()
                @$('#qr-container').toggle()

                if !@$('#qrcode').find('canvas')[0] and @challenge and @sessionKey
                    console.log 'sessionKey', @sessionKey
                    console.log 'challenge', @challenge 
                    @$('#qrcode').qrcode({ width: 190,height: 190,text: "moobiMe://www.moobi.es/"+@sessionKey+"/"+@challenge})
            else
                app.popover 
                    code: 401
                    message: 'failure'



    googleServShow: ->
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('#googleServLi').addClass('activeLoginlft-left')
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#googleServBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#googleServ > H2').fadeIn(500)
        $('#googleServ > .normal > .normalContainer').fadeIn(500)
        $('#googleServ').css('z-index':'6')
        
    linkInShow: ->
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('#linkedLi').addClass('activeLoginlft-right')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#linkedBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#linkedIn > H2').fadeIn(500)
        $('#linkedIn > .normal > .normalContainer').fadeIn(500)
        $('#linkedIn').css('z-index':'6')
        
    twitterShow: ->
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('#twitLi').addClass('activeLoginlft-right')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#twitBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#twitter > H2').fadeIn(500)
        $('#twitter > .normal > .normalContainer').fadeIn(500)
        $('#twitter').css('z-index':'6')

    facebookShow: ->
        $('.toHide-lft').removeClass('activeLoginlft-left')
        $('.toHide-rht').removeClass('activeLoginlft-right')
        $('#facebookLi').addClass('activeLoginlft-right')
        $('.authChose').css('z-index':'5')
        $(".chooseBut").css('background-position','100% 0px')
        $("#facebookBut").css('background-position','100% -60px')
        $('.authChose > H2').fadeOut(300)
        $('.authChose > .normal > .normalContainer').fadeOut(300)
        $('#facebook > H2').fadeIn(500)
        $('#facebook > .normal > .normalContainer').fadeIn(500)
        $('#facebook').css('z-index':'6')

    showForgotPass:(e) ->
        console.log 'forgot pass'
        e.preventDefault()
        $('.fld').slideUp('1500')
        $('.forgotPass').show('1500')

    forgotPass:(e)->
        console.log 'submit'
        e.preventDefault()
        $.post '/auth/forgot',
            check: $('#forgotPassword').val()
        , =>
            $('#forgotPassword').val('')
            @showForgotPass(e)
            app.popover  
                code: 200
                message: 'message sent'
