AuthView = Backbone.View.extend

    initialize: (options)->
        console.log "LoginView init"
        @action = options.action
        @views = {}
        $.get '/templates/auth/auth.html', (html)=>
            @template = _.template(html)
            @$el.html @template
                nls: NLS
            @render()
            @show()
            #@model.on 'info', @render, @

    show: ->
        console.log 'AuthView show'
        @$('.nav-tabs li').removeClass('active')
        @$("a[href$='#auth/#{@action}']").parent().addClass('active')
        @$('.authModule').hide()
        app.$('.appModule').hide()
        @views[@action].show() if @views[@action]
        @$el.show()

    render: ->
        if @template
            console.log 'AuthView render'
            #if @model.get('roles') and @model.get('roles').indexOf('user') isnt -1
                #@$('.hidden').show()
            #else
                #@$('.hidden').hide()
            if @model.get('user').roles and @model.get('user').roles.indexOf('user') isnt -1
                @$('a[href$="join"]').hide()
                @$('a[href$="issuer"]').show()
                @$('a[href$="awarder"]').show()
                @$('a[href$="applications"]').show()
                @$('a[href$="profile"]').show()
            
            
            #
            else    
                console.log '4.1'
                @$('a[href$="join"]').show()
                @$('a[href$="issuer"]').hide()
                @$('a[href$="awarder"]').hide()
                @$('a[href$="applications"]').hide()
                @$('a[href$="profile"]').hide()
                
            unless @views[@action]
                switch @action
                    when 'join'
                        @views[@action] = new AuthJoinView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                    when 'oauth'
                        @views[@action] = new AuthOauthView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                    
                    when 'main'
                        @views[@action] = new MainAuthView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                
                    when 'issuer'
                        @views[@action] = new AuthIssuerView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                    when 'awarder'
                        @views[@action] = new AuthAwarderView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                    when 'applications'
                        @views[@action] = new AuthApplicationsView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')
                    when 'profile'
                        @views[@action] = new AuthProfileView
                            model: @model
                            el: $('<div class=authModule />').appendTo @$('#auth-body')

            else
                @views[@action].render()
