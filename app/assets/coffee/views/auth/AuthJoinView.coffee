AuthJoinView = Backbone.View.extend

    events:
        "submit #join-form": "join"
        #"click #join-form": "join"
        #"submit #login-form": "login"
        #"click #login-form": "login"
        #"click #login-facebook": "loginSocial"
        #"click #button-awarder-form": "awarderForm"
        #"click #button-issuer-form": "issuerForm"
        #"click #button-application-list": "applicationList"
        #"click #issuer-submit": "issuerFormSave"
        #"click #awarder-submit": "awarderFormSave"
        #"click .forgotPass": "showForgotPass"
        #"submit #pass-reset": 'forgotPass' 

    initialize: ->
        console.log 'AuthJoinView init'
        $.get '/templates/auth/user.html', (html)=>
            @template = _.template(html)
            console.log '5'
            @render()

    show: ->
        @$el.show()

    render: ->
        console.log 'AuthUserView render'
        if @template
            console.log '6'
            @$el.html @template
                nls: NLS
                user: @model.get('user')
            @$joinForm = @$("#join-form")
            @$joinSubmit = @$("#join-submit")
            @$loginForm = @$("#login-form")
            @$loginSubmit = @$("#login-submit")
            @rendered = true

    #login: (e) ->
        #e.preventDefault()
        #data = @$loginForm.serializeObject()
        #if @validateLogin data
            #@$loginSubmit.button "loading"
            #@model.login data, (err) =>
                #console.log "Login has errors", err if err

                ##bad credetials error
                #if err
                    #$inputs = @$el.find('form#login-form').find("input")
                    #$block = $inputs.closest(".control-group")
                    #$block.addClass("error").find(".error-message").text('bad credentials').show()
                    #$inputs.bind "change.validate keyup.validate", ->
                        #$block.removeClass("error").find(".error-message").hide()
                        #$inputs.unbind ".validate"

                #@$loginForm.find('input').val('') unless err
                #@$loginSubmit.button "reset"

        #false

    #validateLogin: (data)->
        #rUsername = /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/
        #errors = []

        #if data.login is ''
            #errors.push
                #name: "username"
                #error: "Cant be empty"
        #else
            #unless rUsername.test(data.login)
                #errors.push
                    #name: "login"
                    #error: "Usernames must start only with a letter," + "may only contain letters and numbers." + "Username must have 1 to 48 letters length."

        #if data.password is ''
            #errors.push
                #name: "password"
                #error: "Cant be empty"

        #if errors[0]
            #console.log 'LoginView login errors', errors
            #$inputs = @$el.find('form#login-form').find("input")
            #_.each errors, (error) ->
                #$input = $inputs.filter("[name=\"" + error.name + "\"]")
                #$block = $input.closest(".control-group")
                #$block.addClass("error").find(".error-message").text(error.error).show()
                #$input.bind "change.validate keyup.validate", ->
                    #$block.removeClass("error").find(".error-message").hide()
                    #$input.unbind ".validate"

            #console.log 'validate false'
            #false
        #else
            #console.log 'validate true'
            #true

    join: (e)->
        console.log 'LoginView join' if app.debug
        e.preventDefault()
        data = @$joinForm.serializeObject()
        if @validateJoin(data)
            @$joinSubmit.button "loading"
            @model.join data, (res) =>
                if res.status is 200
                    app.popover { message: "please confirm your e-mail"}

                    
                else
                    @$joinForm.find('input').val('')
                    console.log "Login has errors", res

                    console.log 'reset button'
                    @$joinSubmit.button "reset"
                

        return false


    validateJoin: (data) ->
        rEmail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
        rLogin = /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/
        rPassword = /.{5,20}/#/((?=.*\d).(?!.*\W).{5,20})/ 
        errors = []
        unless rLogin.test(data.login)
            errors.push
                name: "login"
                error: "Logins must start only with a letter," + "may only contain letters and numbers." + "Login must have 1 to 48 letters length."

        #unless data.email or rEmail.test(data.email)
            #errors.push
                #name: "email"
                #error: "Please enter a valid email address"

        unless rPassword.test(data.password) 
            errors.push
                name: "password"
                error: "Password must have at least 6 characters"

        unless data.password2 or data.password isnt data.password2
            errors.push
                name: "password2"
                error: "Passwords must match."

        if errors[0]
            console.log 'LoginView validateJoin, errors:',errors
            @joinErrors $(@el), errors
            false
        else
            true


    joinErrors: ($el, errors) ->
        $inputs = $el.find('form#join-form').find("input")
        _.each errors, (error) ->
            $input = $inputs.filter("[name=\"" + error.name + "\"]")
            $block = $input.closest(".control-group")
            $block.addClass("error").find(".error-message").text(error.error).show()
            $input.bind "change.validate keyup.validate", ->
                $block.removeClass("error").find(".error-message").hide()
                $input.unbind ".validate"

    
    #showForgotPass: (e)->
        #e.preventDefault()
        #if $('#passReset').attr('class') is 'hide'
            #$('#passReset').attr('class', 'show')
        #else
            #$('#passReset').attr('class', 'hide')

    #forgotPass:  (e)->
        #that = @
        #e.preventDefault()
        #$.post '/auth/forgot',
            #check: $('#forgotPassword').val()
        #, ->
            #$('#forgotPassword').val('')
            #that.show_forgotPass(e)
            #app.popover  
                #code: 200
                #message: 'message sent'
            

