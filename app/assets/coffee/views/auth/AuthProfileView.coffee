AuthProfileView = Backbone.View.extend

    events:
        "submit #profile-submit": "profileApplication"
        "click #profile-submit": "profileApplication"

    initialize: ->
        $.get '/templates/auth/profile.html', (html)=>
            @template = _.template(html)
            @render()

    show: ->
        @$el.show()

    render: ->
        if @template
            @$el.html @template
                nls: NLS
                user: @model.get('user')
            @rendered = true

    profileApplication: (e)->
        e.preventDefault()
        data = @$('#profile-form').serializeObject()
        $.post "/api/v0/user/#{@model.get('id')}", data, (res)=>
            if res.users and res.users[0]
                app.popover 
                    code: 200
                    message: 'success'
                @model.set 'user', res.users[0]
                @render()
            else
                app.popover res.error
        false

