AuthIssuerView = Backbone.View.extend


    events:
        "click #issuer-submit": "issuerApplication"
        "submit #issuer-submit": "issuerApplication"


    initialize: ->
        @issuer = {}

        $.get  "/api/v0/issuer/#{@model.get('id')}", (res)=>
            if res.status is 200 and res.issuers[0]
                @issuer = res.issuers[0]
                @render() if @template
                console.log 'Get \n issuer:',@issuer

        $.get '/templates/auth/issuer.html', (html)=>
            @template = _.template(html)
            @render() if @issuer


    show: ->
        @$el.show()


    render: ->
        if @template and @issuer
            @$el.html @template
                nls: NLS
                user: @model.toJSON()
                issuer: @issuer
            @rendered = true


    issuerApplication: (e)->
        e.preventDefault()
        data = @$('#issuer-form').serializeObject()

        $.post "/api/v0/issuer/#{@model.get('id')}", data, (res)=>
            if res.issuers and res.issuers[0]
                app.popover 
                    code: 200
                    message: 'success'
                @issuer = res.issuers[0]
                @render()
            else
                app.popover res.error

        false
