AuthOauthView = Backbone.View.extend

    events:
        "click #login-show-qr":"loginWithQr"
        "click #login-update-qr-status":"updateQrStatus"
        "click #login-twitter": "loginTwitter"
        "click #login-google": "loginGoogle"
        "click #login-linked-in": "loginLinkedIn"
        "click #login-facebook": "loginFacebook"
        "click #login-qr-emulate": "emulateMobileSendsQR"

    initialize: ->
        console.log 'AuthOauthView init'
        $.get '/templates/auth/oauth.html', (html)=>
            @template = _.template(html)
            @model.on 'change', =>
                @rendered = false
            @render()

    show: ->
        @$el.show()

    render: ->
        console.log 'AuthOauthView render'
        if !@rendered and @template
            @$el.html @template
                nls: NLS
                user: @model.get('user')
            @rendered = true

    loginLinkedIn: ->
        console.log 'LoginView loginLinkedIn'
        window.location = "/oauth/linkedin?token=#{@model.get('token')}"
        false

    loginTwitter: ->
        console.log 'LoginView loginTwitter'
        window.location = "/oauth/twitter?token=#{@model.get('token')}"
        false

    loginFacebook: ->
        console.log 'LoginView loginFacebook'
        window.location = "/oauth/facebook?token=#{@model.get('token')}"
        false

    loginGoogle:->
        console.log 'login google'
        window.location = "/oauth/google?token=#{@model.get('token')}"
        false
        
    updateQrStatus: ->
        console.log "AuthView update qr status"
        @model.update()

    emulateMobileSendsQR: (e)->
        console.log "emulate"
        console.log 'device token',$(e.currentTarget).parent().find('input').val()
        body = {}
        body.access_token = $(e.currentTarget).parent().find('input').val()
        body.qr = @qrCode
        console.log 'emulate body:',body

        #$.ajax
            #headers:
                #authorization: "Bearer "+$(e.currentTarget).parent().find('input').val()
            #type: "POST"
            #url: '/auth/qr'
            #data: body
            #success: (res)=>
                #console.log res
                #if res.status is 200
                    #@model.set('token', @qrToken)
            #dataType: dataType

        $.post '/auth/qr', body, (res)=>
            console.log res
        
    updateQrStatus: ->
        console.log "AuthView update qr status"

    loginWithQr:->
        console.log 'getting qr'
        $.get '/qr', (res)=>
            console.log 'loginwithqr res:',res
            #if res.token
            if res.challenge and res.sessionKey
                #@qrCode = res.token
                @challenge = res.challenge
                @sessionKey = res.sessionKey

                console.log 'AuthView login with qr'
                @socket = io.connect(/qr-session/+@sessionKey)
                @socket.on 'connect', (socket)=>
                    console.log 'new connection'
                    @socket.on 'success', (body)=>
                        @model.set('token', body.token)
                        @socket.disconnect()
                @$('#qr-container').toggle()

                if !@$('#qrcode').find('canvas')[0] and @challenge and @sessionKey
                    console.log 'sessionKey', @sessionKey
                    console.log 'challenge', @challenge 
                    @$('#qrcode').qrcode({ width: 128,height: 128,text: "moobiMe://www.moobi.es/"+@sessionKey+"/"+@challenge})
            else
                app.popover 
                    code: 401
                    message: 'failure'

