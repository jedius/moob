AuthApplicationsView = Backbone.View.extend
    events:
        "click #save-submit": "save"
        "submit #application-form": "save"
        "click .update": "update"
        "click .delete": "delete"

    initialize: (obj)->
        $.get '/templates/auth/applications.html', (html)=>
            @template = _.template(html)
            @collection = new ApplicationCollection
            #@active = obj.active if obj.active?
            @fetch()
            @collection.on 'reset', @render, @
    fetch: ->
        $.get '/api/v0/application', (res)=>
            if res and res.applications
                @collection.reset res.applications
            console.log res
            
    render: ->
        if @collection and @template
            @$el.html @template
                nls: NLS
                applications: @collection.toJSON()
                #active: @active
        #app.$('.appModule').hide()
    show:->
        @$el.show()

    save: (e)->
        e.preventDefault()
        data = @$('#application-form').serializeObject()
        $.post '/api/v0/application', data, (res)=>
            if res.applications and res.applications[0]
                app.popover
                    code: 200
                    message: 'success'
                @collection.add new ApplicationModel res.applications[0]
                @render()
            else
                app.popover res.error
        e.preventDefault()
        false

    update: (e)->
        appId = $(e.currentTarget).attr '_id'
        data = 
            redirect: $(e.currentTarget).parents('.application-tr').find('.application-redirect').val()
            name: $(e.currentTarget).parents('.application-tr').find('.application-name').val()
        console.log 'app update data',data
        @collection.get(appId).save data

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'EntityView delete, id:', id
        if confirm 'are you sure?'
            @collection.get(id).destroy()
            @render()
