AuthAwarderView = Backbone.View.extend

    events:
        "click #awarder-submit": "awarderApplication"
        "submit #awarder-submit": "awarderApplication"

    initialize: ->
        @awarder = {}
        $.get  "/api/v0/awarder/#{@model.get('id')}", (res)=>
            if res.status is 200 and res.awarders[0]
                @awarder = res.awarders[0]
                @render() if @template

        $.get '/templates/auth/awarder.html', (html)=>
            @template = _.template(html)
            @render() if @awarder

    show: ->
        @$el.show()

    render: ->
        if @template and @awarder
            @$el.html @template
                nls: NLS
                user: @model.toJSON()
                awarder: @awarder
            @rendered = true

    awarderApplication: (e)->
        e.preventDefault()
        data = @$('#awarder-form').serializeObject()
        $.post "/api/v0/awarder/#{@model.get('id')}", data, (res)=>
            console.log 'Awarder save'
            if res.awarders and res.awarders[0]
                app.popover 
                    code: 200
                    message: 'success'
                @awarder = res.awarders[0]
                @render()
            else
                app.popover res.error
          false

