AppView = Backbone.View.extend

  
    initialize: ->
        console.log 'AppView init1' if @debug
        @debug = true
        @popcount = 0
        @load = 0
        @loaded = 0
        $.get '/templates/app.html', (html)=>
            @template = _.template(html)
            @render() if @template and @panelTemplate and @started
        $.get '/templates/panel.html', (html)=>
            @panelTemplate = _.template(html)
            @render() if @template and @panelTemplate and @started
        @router = new Router()

    start: ->
        @started = true
        @render() if @template and @panelTemplate and @started

    render: ->
        console.log 'AppView render' if @debug
        @$el.html @template
            nls: NLS
        @body = @$('#body')
        @panel = @$('#panel')

        $.ajaxSetup
            error: (err, errText) =>
                @popover errText
                console.log '1'
            complete: (a,b,c)=>
                app.loading false
                try
                    data = $.parseJSON(a.responseText)
                    console.log '2'
                    if data.error
                        console.log '3'
                        @popover data.error if @popover
                        if data.status is 401
                            #console.log 'flag'
                            app.router.navigate 'logout', trigger: true

        #authModel init
        @model = new AppModel
        @authModel = new AuthModel
        @authModel.once 'ready', =>
            console.log 'authModel ready'
            @router.start()
        @authModel.on 'info', =>
            console.log 'authModel info'
            @authModel.trigger 'ready'
            @renderPanel()

        if $.cookie('token')
            @authModel.set 'token', $.cookie('token')
        else
            @router.start()
            @renderPanel()

    renderPanel: ->
        console.log 'AppView renderPanel, user:',@authModel.toJSON() if @debug and @authModel
        name = @authModel.get('user').name
        #if name is 'Guest'
            #if @authModel.get('user').twitterName
                #name = @authModel.get('user').twitterName
        @panel.html @panelTemplate
            nls: NLS
            user: @authModel.get('user')
            name: name or ''
        @$("#msg-win").popover
            trigger: 'manual'

    popover: (msg)->
        console.log 'AppView popover, msg:',msg.message
        console.log 'response',msg
        return unless @$("#msg-win")[0]
        @popcount++
        count = @popcount
        @$("#msg-win").attr "data-original-title", msg.message
        @$("#msg-win").popover "show"
        setTimeout =>
            if count is @popcount
                @$("#msg-win").popover "hide"
        , 2500

    loading: (loading)->
        if loading
            @load++
            @$('.loading-container').stop(true,true).fadeIn()
        else
            @loaded++
            if @load is @loaded
                @$('.loading-container').stop(true,true).fadeOut()
            else if @load < @loaded
                @load = 0
                @loaded = 0
                @$('.loading-container').stop(true,true).fadeOut()

