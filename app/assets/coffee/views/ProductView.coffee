ProductView = Backbone.View.extend

    initialize: (obj)->
        console.log 'ChannelView init' if @debug
        @id = obj.id

        $.get '/templates/product/modal.html', (html)=>
            @modalTemplate = _.template(html)
        $.get '/templates/product/product.html', (html)=>
            @template = _.template(html)
            @render()

    render: ->
        console.log 'ChannelView render'
        $('.appModule').hide()
        if @template
            $.get "/api/v0/product/#{@id}", (res)=>
                console.log 'ChannelView getChannel res:',res
                @product = res.products[0]
                @$el.html @template
                    nls: NLS
                    product: @product
                @$el.show()
