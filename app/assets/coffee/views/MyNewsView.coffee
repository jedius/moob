MyNewsView = Backbone.View.extend

    events:
        'click .channelContentRow': 'openChannel'
        'click #dataAccept': 'changeDate'

    initialize: (obj)->
        console.log 'ChannelView init' if @debug
        @collection = new ChannelContentCollection
        @varyDate = false
        console.log @varyDate
        @fetched = false
        app.authModel.on 'change:channels', =>
            @fetched = false
        $.get '/templates/my/news.html', (html)=>
            @template = _.template(html)
            @render()

    fetch: (cb)->
        @fetched = true
        #TODO to pick date and to send in query
        unless @varyDate
            $.get "/api/v0/my/news", (res)=>
                console.log 'ChannelView fetch, get list response:', res
                if res.channelContents
                    for channelContent in res.channelContents
                        @collection.add new ChannelContentModel channelContent
                cb() if cb
        else
            $.get "/api/v0/my/news?date="+@newReqDate, (res)=>
                console.log 'ChannelView fetch, get list response with changed date:', res
                if res.channelContents
                    for channelContent in res.channelContents
                        @collection.add new ChannelContentModel channelContent
                cb() if cb
        
    render: ->
        console.log 'MyNewsView render'
        $('.appModule').hide()
        if @collection and @template
            unless @fetched
                @fetch =>
                    @$el.html @template
                        nls: NLS
                        utils: utils
                        channelContents: @collection.toJSON()
                    @datePicker()    
            else
                @$el.html @template
                    nls: NLS
                    utils: utils
                    channelContents: @collection.toJSON()
                @datePicker() 
        @$el.show()
        
    openChannel: (e)->
        id = $(e.currentTarget).attr 'channelId'
        console.log 'MyNewsView openChannel',id
        if id
            app.router.navigate "channel/#{id}/1", trigger: true
        
    datePicker:->
        @myDate = new Date()
        @month = @myDate.getMonth() + 1
        @creatDate = @month + '/' + @myDate.getDate() + '/' + @myDate.getFullYear()
        $(".newDate").val(@creatDate)
        $(".newDate").datepicker()

    changeDate:->
        @newReqDate = Date.parse($(".newDate").val())
        @varyDate = true
        @fetched = false
        @render()
        console.log @newReqDate
        console.log @varyDate
