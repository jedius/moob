ChannelView = Backbone.View.extend

    events:
        "click .create-content": "createContent"
        "click .update-content": "updateContent"
        "click .save": "save"
        "click .delete": "delete"
        "click .deleteAll": "deleteAll"
        "click .content-open": "openContent"

        #subscribes
        "click .channel-subscribe": "subscribe"
        "click .channel-subscribe-awarder": "subscribeAsAwarder"
        "click .channel-unsubscribe": "unsubscribe"
        "click .channel-unsubscribe-awarder": "unsubscribeAsAwarder"



    initialize: (obj)->
        console.log 'ChannelView init' if @debug
        @collection = new ChannelContentCollection
        @collection.on 'reset', @render, @
        @collection.on 'add', @render, @
        app.authModel.on 'change:channels', @render, @
        #app.model.on 'publish', @render, @

        $.get '/templates/channel/modal.html', (html)=>
            @modalTemplate = _.template(html)
        $.get '/templates/channel/channel.html', (html)=>
            @template = _.template(html)
            @page = obj.page or 1
            @id = obj.id
            @fetch() if @channel

        $.get "/api/v0/channel/#{@id}", (res)=>
            console.log 'ChannelView getChannel res:',res
            
            @channel = res.channels[0]
            @fetch() if @template

    fetch: ->
        console.log 'ChannelView fetch'
        app.router.navigate "channel/#{@id}/#{@page}"
        $.get "/api/v0/channel/#{@id}/content", {skip: (@page-1)*20}, (res)=>
            console.log 'ChannelView fetch, get list response:', res
            @count = res.count
            @pageCount = parseInt(@count/20)+1
            if res.channelContents
                @collection.reset res.channelContents


    render: ->
        console.log 'ChannelView render'
        if @collection and @template
            subscribed = false
            if app.authModel.get('user').channels
                for ch in app.authModel.get('user').channels
                    if ch._id is @id
                        subscribed = true
                    else
                        console.log ch._id, @id

            subscribedAsAwarder = false
            
            if app.authModel.get('awarder')
                console.log 'awarder channels',app.authModel.get('awarder').channels
                if app.authModel.get('awarder').channels
                    for ch in app.authModel.get('awarder').channels 
                        if ch is @id
                            subscribedAsAwarder = true
                        else
                            console.log ch, @id


            console.log 'authModel.user',app.authModel.get('user')

            @$el.html @template
                nls: NLS
                utils: utils
                page: @page
                pageCount: @pageCount
                channel: @channel
                channelContents: @collection.toJSON()
                user: app.authModel.get('user')
                subscribed: subscribed
                subscribedAsAwarder: subscribedAsAwarder

            if @page <= 1
                @$('.pag-left').addClass('disabled')
            if @page >= @pageCount
                @$('.pag-right').addClass('disabled')
            if @page > 1 and @page < @pageCount
                @$('.pag-left, .pag-right').removeClass('disabled')
        $('.appModule').hide()
        @$el.show()


    pagination: (e)->
        if $(e.currentTarget).attr('pag') is 'next'
            if @page < @pageCount
                @page++ 
                console.log 'pagination', @page
                @fetch()
        if $(e.currentTarget).attr('pag') is 'prev'
            if @page > 0
                @page--
                console.log 'pagination', @page
                @fetch()

    createContent: (e)->
        console.log 'ChannelsView createContent'
        @$('.el-modal').html @modalTemplate
            nls: NLS
            utils: utils
            channelContent: {}
        @modal = @$('.modal').modal({ show: true, backdrop: true })
        e.preventDefault()
        false

    updateContent: (e)->
        id = $(e.currentTarget).attr "_id"
        console.log 'ChannelsView updateContent, id', id

        @$('.el-modal').html @modalTemplate
            nls: NLS
            utils: utils
            channelContent: @collection.get(id).toJSON()
        @modal = @$('.modal').modal({ show: true, backdrop: true })
        e.preventDefault()
        false


    save: ->
        data = @$('.modal form').serializeObject()
        console.log 'ChannelsView save, data:', data
        @modal.modal('hide').on 'hidden', =>
            if data.id
                @collection.get(data.id).save data,
                    success: =>
                        console.log 'ChannelsView save success'
                        @fetch()
            else
                $.post "/api/v0/channel/#{@id}/content", data, (res)=>
                    console.log 'ChannelsView save, create response:',res
                    if !res.error and res.channelContents and res.channelContents[0]
                        @fetch()
                        #@collection.add new ChannelContentModel res.channelContents[0]
                        #@render()
                    else
                        app.popover res.error.message
        false

    delete: (e)->
        id = $(e.currentTarget).attr '_id'
        console.log 'EntityView delete, id:', id
        if confirm 'are you sure?'
            @collection.get(id).destroy()
            @render()
        e.preventDefault()
        false

    openContent: (e)->
        entity = $(e.currentTarget).attr 'contentType'
        id = $(e.currentTarget).attr 'contentId'
        console.log 'ChannelView contentOpen', entity, id
        app.router.navigate "#{entity}/#{id}", trigger: true
        e.preventDefault()
        false


    subscribe: ->
        console.log 'channelView subscribe as user' if app.debug
        @$('.channel-subscribe').button "subscribing..."
        app.authModel.subscribe @id

    subscribeAsAwarder: ->
        console.log 'channelView subscribe as awarder' if app.debug
        @$('.channel-subscribe-awarder').button "subscribing..."
        app.authModel.subscribeAsAwarder @id

    unsubscribe: ->
        console.log 'channelView unsubscribe as user' if app.debug
        @$('.channel-subscribe').button "unsubscribing..."
        app.authModel.unsubscribe @id

    unsubscribeAsAwarder: ->
        console.log 'channelView unsubscribe as awarder' if app.debug
        @$('.channel-subscribe-awarder').button "unsubscribing..."
        app.authModel.unsubscribeAsAwarder @id

    deleteAll: ->
        console.log 'channelView deleteAll' if app.debug
        if confirm "are you really want to delete all content?"
            $.ajax
                type: 'DELETE'
                url: "/api/v0/channel/#{@id}/content"
                success: (res)=>
                    console.log 'deleteAll res',res
                    if res.status is 200
                        app.popover 
                            code: 200
                            message: 'success'
                        @fetch()

