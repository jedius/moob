socketIo = glob.modules.socketIo
glob.qrSessions = [] 

logFrom = 'socket.io'

#io = socket.listen glob.modules.http.createServer()
io = socketIo.listen glob.server

io.configure ()->
    io.set "transports", ["xhr-polling"]
    io.set "polling duration", 10
    io.set 'log level',1

io.sockets.on 'connection', (socket)->
    log.info logFrom, 'new connection'
    socket.on 'init', (data)->
        glob.qrSessions[data.token] = socketIo.of('/qr-session/'+data.token)

        socket.emit 'created', (data)->
            console.log 'data from client:',data
        
        socket.emit 'fromServer', {data: 'data from server'}

glob.io = io

glob.createRoom = (token)->
    logFrom = 'WWW createRoom'
    log.info logFrom, 'start, token:'+token
    if token
        io.of("/qr-session/#{token}").on 'connection', (socket)->
            socket.on 'ok', ->
                socket.disconnect()

