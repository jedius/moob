logFrom = 'WWW server: '
global.glob = {}

glob.modules = 
    http: require 'http'
    url: require 'url'
    fs: require 'fs'
    child_process: require 'child_process'
    util: require 'util'
    express: express = require 'express'
    path: path = require 'path'
    #crypto: require 'crypto'
    winston: require 'winston'
    request: require 'request'
    socketIo: require 'socket.io'

glob.config = require '../config'

#glob.utils = require './utils'

glob.modules.winston.addColors
    info: 'blue'
    warn: 'yellow'
    error: 'red'
global.log = new (glob.modules.winston.Logger)(
    transports: [
        new (glob.modules.winston.transports.Console)({level: glob.config.log.level, colorize: 'true'}),
        new (glob.modules.winston.transports.File)({filename: glob.config.log.path})
    ]
    exitOnError: false
)
log.inspect = (logFrom,obj)->
    msg = glob.modules.util.inspect(obj,false,5)
    log.info logFrom, msg

glob.app = app = express()

coffee = (next)->
    #console.log 'coffee'
    glob.modules.child_process.exec "#{__dirname}/../node_modules/coffee-script/bin/coffee -p -j #{__dirname}/js/main.js -c #{__dirname}/assets/coffee/", (err,stdout,stderr)->
        #console.log err,stdout,stderr
        #if err
            #log.error 'WWW coffee err:', err
        if stderr
            #log.error 'WWW coffee stderr:', stderr
            glob.modules.child_process.exec "#{__dirname}/../node_modules/coffee-script/bin/coffee -p -c #{__dirname}/assets/coffee/", (err,stdout,stderr)->
                console.log 'coffee err: ',stderr
            next() if next
        else
            glob.modules.fs.writeFile "#{__dirname}/public/js/main.js", stdout, ->
                next() if next

less = (next)->
    glob.modules.child_process.exec "#{__dirname}/../node_modules/less/bin/lessc #{__dirname}/assets/less/main.less", (err,stdout,stderr)->
        if err
            log.error 'WWW less err:', err
            if stderr
                log.error 'WWW less stderr:', stderr
            next() if next
        else if stderr
            log.error 'WWW less stderr:', stderr
            next() if next
        else if stdout
            glob.modules.fs.writeFile __dirname+'/public/css/main.css', stdout, ->
                next() if next

app.configure ->
    app.use express.favicon()
    app.use express.cookieParser()
    app.use express.bodyParser()
    app.use express.methodOverride()
    if process.env.NODE_ENV is 'testing'
        coffee()
        less()
    else if process.env.NODE_ENV is 'production'
    else
        app.use (req,res,next)->
            if req.url is '/js/main.js'
                coffee next
            else if req.url is '/css/main.css'
                less next
            else
                next()
    app.use app.router
    app.use express.static(path.join(__dirname, 'public'))

require './errors'


app.configure "development", ->
    app.use express.errorHandler
        dumpExceptions: true
        showStack: true

app.configure "production", ->
    app.use express.errorHandler
        dumpExceptions: false
        showStack: false

app.all '/api/*', (req,res)->
    log.info "WWW to #{req.method} API:", req.url
    req.url = req.url.substr 4
    console.log glob.config.api.url+req.url
    request = {}
    request.url = glob.config.api.url+req.url
    if req.headers and req.headers.authorization
        request.headers = 
            authorization: req.headers.authorization
    if req.body
        request.body = req.body
    request.method = req.method
    request.json = true
    #console.log request
    glob.modules.request request, (err,response,body)->
        #log.error 'WWW app.all /api/'+req.url, err if err
        unless err
            if body 
                if response and response.statusCode is 200
                    res.send body
                else
                    res.send
                        status: 400
                        error: body
            else if response
                res.send 
                    status: response.statusCode
            else
                res.send {status: 500, error: 'no response from api'}
        else
            log.error 'WWW app.all /api/'+req.url
            res.send 
                status: 500
                error: glob.errors['501']

app.all '/auth/*', (req,res)->
    req.url = req.url.substr 5
    logFrom = "WWW to #{req.method} Auth: #{req.url}" 
    log.info logFrom, 'start'
    console.log req.body
    request = {}
    request.url = glob.config.auth.url+req.url
    if req.headers['accept-language']
        request.headers =
            'accept-language': req.headers['accept-language']
    if req.headers.authorization
        request.headers = 
            authorization: req.headers.authorization
    if req.body
        request.body = req.body
    request.method = req.method
    request.json = true
    #console.log request
    glob.modules.request request, (err,response,body)->
        #console.log body
        if err
            log.error 'WWW app.all /auth/'+req.url, err if err
            res.send 500
        else if body
            log.info logFrom, 'body sent to auth'
            res.send body
        else if response
            log.info logFrom, response.statusCode
            res.send response.statusCode
        else
            res.send 500
            

app.all '/oauth/*', (req,res)->
    req.url = req.url.substr 6

    res.redirect glob.config.auth.url+req.url+"&back=#{glob.config.app.url}&token=#{req.cookies.token}"

html = glob.modules.fs.readFileSync __dirname+'/public/index.html'
length = html.length
app.get '/', (req,res)->
    if req.query.token
        res.cookie 'token', req.query.token
        res.redirect '/'
    else
        res.setHeader 'Content-Type', 'text/html'
        res.setHeader 'Content-Length', length
        res.end html

app.get '/qr', (req,res)->
    console.log '/qr'
    authRequest =
        method: 'GET'
        url: glob.config.auth.url+'/qr'
        json: true
    glob.modules.request authRequest, (error, response,body) ->
        console.log '/qr body:',body
        log.info logFrom, body
        if body? and body.challenge and body.sessionKey
            console.log body.challenge
            console.log body.sessionKey
            glob.qrSessions[body.sessionKey] = glob.createRoom body.sessionKey
            res.send
                status: 200
                challenge: body.challenge
                sessionKey: body.sessionKey
        else
            console.log '/qr no body or token'
            console.log response.statusCode
            console.log body
            res.send
                status: 500

app.post '/qrAuthAnswer', (req,res)->
    console.log 'qrauthanswer token',req.body.token
    console.log 'qrauthanswer qrCode',req.body.sessionKey
    if req.body.sessionKey?
        if glob.qrSessions[req.body.sessionKey]
            log.info logFrom, 'emit to client with session: '+req.body.sessionKey
            glob.qrSessions[req.body.sessionKey].emit 'success', {token: req.body.token}
            res.send 200
        else
            res.send 400
    else
        res.send 400
        
glob.server = glob.modules.http.createServer(app).listen glob.config.app.port, ->
    log.info 'WWW server start on port '+glob.config.app.port+' in '+app.settings.env+' mode'
    console.log 'WWW server is ready' 

require './socket'
