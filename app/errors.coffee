#console.log 'errors'
glob.errors =

    #manipulating with db
    '1000': 
        code: 1000
        message: 'Common error of object changing'
    '1001': 
        code: 1001
        message: 'Created object already exists'
    '1002': 
        code: 1002
        message: 'Requested object does not exist'
    '1003': 
        code: 1003
        message: 'Deleted object does not exist'
    '1004': 
        code: 1004
        message: 'Deleted object can not be deleted'
    
    '1100':
        code: 1100
        message: 'user with this login or email not found'


    #about objects

    #0 - not found
    #1 - already exist
    #2 - expired
    #3 - 
    #4 - save error
    #5 - find error

    '20000': 
        code: 20000
        message: 'Requested object does not exist'
    '20100': 
        code: 20100
        message: 'application not found'
    '20200': 
        code: 20200
        message: 'awarder not found'
    '20300': 
        code: 20300
        message: 'channel not found'
    '20301':
        code: 20301
        message: 'channel already exist'

    '20400': 
        code: 20400
        message: 'checkins not found'
    '20500': 
        code: 20500
        message: 'issuer not found'
    '20600': 
        code: 20600
        message: 'location not found'

    '20700': 
        code: 20700
        message: 'loyalty not found'
    '20704': 
        code: 20704
        message: 'loyalty save error'
    '20705': 
        code: 20705
        message: 'loyalty find error'


    '20710': 
        code: 20710
        message: 'loyalty template not found'
    '20711': 
        code: 20711
        message: 'loyalty template already exist'
    '20712': 
        code: 20712
        message: 'loyalty template expired'
    '20714': 
        code: 20714
        message: 'loyalty template save error'
    '20715': 
        code: 20715
        message: 'loyalty template find error'


    #1 - not found
    #2 - already exist
    #3 - expired
    #4 - save error
    #5 - find error

    '20800': 
        code: 20800
        message: 'offer not found'

    '20900': 
        code: 20900
        message: 'payway not found'
    '20901': 
        code: 20901
        message: 'payway already exist'
    '20905': 
        code: 20905
        message: 'payway find error'

    '20910': 
        code: 20910
        message: 'payway template not found'
    '20911': 
        code: 20911
        message: 'payway template already exist'
    '20912': 
        code: 20912
        message: 'payway template expired'
    '20914': 
        code: 20914
        message: 'payway template save error'
    '20915': 
        code: 20915
        message: 'payway template find error'


    '21000': 
        code: 21000
        message: 'product not found'

    '21100': 
        code: 21100
        message: 'rule not found'

    #unicode
    '21200': 
        code: 21200
        message: 'unicode not found'
    '21201': 
        code: 21201
        message: 'unicode already exist'

    '21205': 
        code: 21205
        message: 'unicode find error'



    '21210': 
        code: 21210
        message: 'unicode status isnt blocked'
    '21211': 
        code: 21211
        message: 'unicode status isnt available'
    '21212':
        code: 21212
        message: 'code already exist, please try again'

    '21300': 
        code: 21300
        message: 'user not found'
    '21301': 
        code: 21301
        message: 'user already exist'
    '21302': 
        code: 21302
        message: 'user hasnt device info'
    '21303': 
        code: 21303
        message: 'user has unsupported os'
    '21304': 
        code: 21304
        message: 'user find error'
    '21305': 
        code: 21305
        message: 'user save error'
    '21306': 
        code: 21306
        message: 'user save error'
    '21307': 
        code: 21307
        message: 'get user error'
    '21310': 
        code: 21310
        message: 'user has not payways'

    '21400':
        code: 21400
        message:"channelContent not found"

    '21500':
        code: 21500
        message: 'login not found'
    '21501':
        code: 21501
        message: 'login already exist'
    '21502':
        code: 21502
        message: 'email already registred'

    '21600':
        code: 21600
        message: 'password not found'
    '21603':
        code: 21603
        message: 'password expired'


    '21700':
        code: 21700
        message: "template not found"

    '21701':
        code: 21701
        message: "template already exist"

    '21800':
        code: 21800
        message: "qrCode not found"
    '21801':
        code: 21801
        message: "qrCode already exist"
    '21802':
        code: 21802
        message: "qrCode expired"


    '21900':
        code: 21900
        message: "token not found"


    '22000':
        code: 22000
        message: "client not found"
    '22001':
        code: 22001
        message: "client already exist"
    '22002':
        code: 22002
        message: "client already exist"
    '22003':
        code: 22003
        message: "client expired"
    '22004':
        code: 22004
        message: "client save error"
    '22005':
        code: 22005
        message: "client find error"
    '22006':
        code: 22006
        message: "invalid type of client registration"


    '22100':
        code: 22100
        message: "payment not found"
    '22101':
        code: 22101
        message: "payment already exist"
    '22104':
        code: 22104
        message: "payment save error"
    '22105':
        code: 22105
        message: "payment find error"
    '22106':
        code: 22106
        message: "payment update error"

    '22110':
        code: 22110
        message: "payment status has no created state"

    #identity
    '23100':
        code: 23100
        message: "identity not found"
    '23101':
        code: 23101
        message: ""
    '23102':
        code: 23102
        message: ""
    '23103':
        code: 23103
        message: ""
    '23104':
        code: 23104
        message: "identity save error"
    '23105':
        code: 23105
        message: "identity find error"
    '23106':
        code: 23106
        message: "identity creation error error"

    '23200':
        code: 23200
        message: "receipt not found"
    '23201':
        code: 23201
        message: "receipt not found"
    '23202':
        code: 23202
        message: "receipt not found"
    '23203':
        code: 23203
        message: "receipt not found"
    '23204':
        code: 23204
        message: "receipt save error"
    '23205':
        code: 23205
        message: "receipt find error"
    '23206':
        code: 23206
        message: "receipt update error"


    '23300':
        code: 23300
        message: "event not found"
    '23304':
        code: 23304
        message: "event save error"
    '23305':
        code: 23305
        message: "event find error"
    '23306':
        code: 23306
        message: "event update error"

    '23400':
        code: 23400
        message: "transportationTicket not found"
    '23404':
        code: 23404
        message: "transportationTicket save error"
    '23405':
        code: 23405
        message: "transportationTicket find error"
    '23406':
        code: 23406
        message: "transportationTicket update error"


    #access
    '4000': 
        code: 4000
        message: 'Access error'
    '4001': 
        code: 4001
        message: 'Authentication Required'

    #roles 4010 - 4200
    '4010': 
        code: 4010
        message: 'User has not a role of awarder'
    '4011': 
        code: 4011
        message: 'User has not a role of issuer'
    '4012': 
        code: 4012
        message: 'Guest has not a role of user'
    '4013': 
        code: 4013
        message: 'Guest has not a role of user'

    '4014':
        code: 4014
        message: "only owner or admin can update"

    
    #issuer doesnt own this offer
    #only owner or admin can update application
   
    '4015':
       code:4015
       message:"only admin can delete awarder"

    '4016':
       code:4016
       message: "only issuer and admin can create channel"
    #only issuer and admin can create channel
    #user not an issuer
    #awarder already subscribed
    #awarder is not awarder
    '4017':
        code:4017
        message:"only channel owner or admin can post news"

    '4018':
        code:4018
        message:"only channel owner or admin can update news"

    '4019':
        code:4019
        message: "awarder is not subscribed"

    '4020':
        code:4020 
        message:"issuer not owner of channel"

    '4021':
        code:4021
        message:"validation by rules has failed"

    '4022':
        code:4022
        message:'issuer is not active'
    #removes the user is not subscribed on channel
    #not subscribed channels

    #arguments of request


    #invalid query
    '5000': 
        code: 5000
        message: 'invalid arguments of request'
        
    '5001': 
        code: 5001
        message: 'invalid id'

    '5002':
        code: 5002
        message: 'invalid applicationId'

    '5003':
        code: 5003
        message: 'invalid awarderId'

    '5004':
        code: 5004
        message: 'invalid userId'

    '5005':
        code: 5005
        message:'invalid chanel name'

    '5006':
        code: 5006
        message:'invalid chanelId'

    '5007':
        code:5007
        message:'invalid chekinsId'

    '5008':
        code:5008
        message:'invalid issuerId'

    '5009':
        code: 5009
        message: 'invalid login'

    '5010':
        code: 5010
        message: 'invalid productId'
    
    '5011':
        code: 5011
        message: 'invalid old password'

    '5012':
        code: 5012
        message: 'invalid locationId'

    '5013':
        code: 5013
        message: 'invalid password'

    '5014':
        code:5014
        message: 'invalid loyaltyId'

    '5015':
        code:5015
        message: 'invalid templateId'

    '5016':
        code: 5016
        message: 'invalid offerId'

    '5017':
        code: 5017
        message: 'invalid unicodeId'
    
    '5018':
        code: 5018
        message: "invalid date"

    '5019':
        code: 5019
        message: 'bad paywayId'

    '5020':
        code: 5020
        message: 'invalid token'

    '5021':
        code: 5021
        message: 'invalid registrationId'

    '5022':
        code: 5022
        message: 'registrationId required'

    '5023':
        code: 5023
        message: 'invalid collapseKey'

    '5024':
        code: 5024
        message: 'collapseKey required'

    '5025':
        code: 5025
        message: 'data must be array of {key: "key12saw2", value: "value123sa2"}'

    '5026':
        code: 5026
        message: 'data required'

    '5027':
        code: 5027
        message: 'invalid dataId'

    '5028':
        code: 5028
        message: 'invalid paymentId'

    '5029':
        code: 5029
        message: 'invalid transactionId'

    '5030':
        code: 5030
        message: 'invalid url'

    '5031':
        code: 5031
        message: 'invalid amount'

    '5032':
        code: 5032
        message: 'invalid name'

    '5033':
        code: 5033
        message: 'invalid short or full description'

    '5034':
        code: 5034
        message: 'invalid status'

    '5035':
        code: 5035
        message: 'invalid category'

    '5036':
        code: 5036
        message: 'invalid confirm password'

    '5037':
        code: 5037
        message: 'invalid zip code'


    '5038':
        code: 5038
        message: 'invalid email'


    #DB errors
    '5100':
        code: 5100
        message: 'mobile push failed'
    
    #'5100':
        #code: 5100

    #DB errors
    '500': 
        code: 500
        message: 'DB error'
    '501': 
        code: 501
        message: 'internal server error'


    '400':
        code: 400
        message:'bad request'

    '404':
        code: 404
        message:'not found'


    #modules
    '6100': 
        code: 6100
        message: 'gcm is disabled'
    '6101': 
        code: 6101
        message: 'gcm sendind failure'
    '6102': 
        code: 6102
        message: 'apn sendind failure'
    '6103': 
        code: 6103
        message: 'mpns sendind failure'

    '6110':
        code: 6110
        message: 'gcm is disabled'

    '6120':
        code: 6120
        message: 'mpns is disabled'

    

