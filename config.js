config = {
    app: {
        url: 'http://localhost:3600',
        name: 'MoobWWW',
        port: 3600,
        secretString: 'secretString',
    },
    log: {
        level: 'info',
        path: __dirname+'/www.log'
    },
    auth: {
        url: 'http://localhost:3000'
    },
    api: {
        url: 'http://localhost:3500'
    },
    db: {
        mongoUrl: 'mongodb://localhost/moob',
        redisUrl: 'redis://localhost:6379'
    }, 
    validate: {
        email: /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        string: /^([a-zA-Z]){1}([a-zA-Z0-9]){0,48}$/,
        objectId: /^([a-z0-9]){24}$/,
        password: /.{5,20}/,
        url: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
    },
    secret: 'secret'
};

if (process.env.NODE_ENV == 'production') {
    config.app.port = process.env.PORT;
    //config.db.mongoUrl = process.env.MONGOHQ_URL;
    config.db.mongoUrl = 'mongodb://heroku:8da7e3e135eaa2a2808b82799f0fc48d@alex.mongohq.com:10047/app9753282'
    config.db.redisUrl = process.env.REDISTOGO_URL
    config.log.level = 'info';
    config.log.path = './www.log'
    config.app.url = 'http://www-moob.herokuapp.com'
    config.auth.url = 'http://auth-moob.herokuapp.com'
    config.api.url = 'http://api-moob.herokuapp.com'

} else if  (process.env.NODE_ENV == 'development') {
} else if  (process.env.NODE_ENV == 'testing') {

    config.db.mongoUrl = 'mongodb://localhost/moobTests'
    //config.db.mongoUrl = 'mongodb://moobi_db:&42U(?s#-W@ds037007.mongolab.com:37007/moobi'
    //config.db.mongoURL = "mongodb://db_user:4you2Know@ds037007-a.mongolab.com:37007/moobi";
    config.app.url = 'http://localhost:4600'
    config.app.port = 4600
    config.api.url = 'http://localhost:4500'
    config.auth.url = 'http://localhost:4000'

};

module.exports = config;
